package com.ibm.meledina.sample.mfp.service;

import com.ibm.meledina.sample.mfp.dto.ResponseDTO;

public class SampleService {

	private final String responseString = "... back to you!";
	
	public ResponseDTO sampleOperation(String inputString) {
		String outputString = inputString + responseString; 
		System.out.println("Received string " + inputString + " as input parameter... returning '" + outputString + "'");
		
		return new ResponseDTO(true,outputString);
	}
}
