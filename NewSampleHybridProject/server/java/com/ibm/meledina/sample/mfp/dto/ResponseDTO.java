package com.ibm.meledina.sample.mfp.dto;

import com.google.gson.Gson;

public class ResponseDTO {

	private Boolean successful;
	private String message;
	
	public ResponseDTO() {}
	
	public ResponseDTO(Boolean success, String message) {
		this.successful = success;
		this.message = message;
	}

	public Boolean isSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean success) {
		this.successful = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String toJSON() {
		return (new Gson()).toJson(this);
	}
	
	@Override
	public String toString() {
		return this.toJSON();
	}
}
