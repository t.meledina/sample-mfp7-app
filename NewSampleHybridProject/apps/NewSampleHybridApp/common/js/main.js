/// init ///

function wlCommonInit(){
	/*
	 * Use of WL.Client.connect() API before any connectivity to a MobileFirst Server is required. 
	 * This API should be called only once, before any other WL.Client methods that communicate with the MobileFirst Server.
	 * Don't forget to specify and implement onSuccess and onFailure callback functions for WL.Client.connect(), e.g:
	    
	 */   
	WL.Client.connect({
		onSuccess: onConnectSuccess,
		onFailure: onConnectFailure
	 });
}

/// public ///

function callService() {
	var dummyInput = "dummy-from-client";
	console.log("About to call service with input " + dummyInput);

	var invocationData = {
		adapter : "SampleAdapter",
		procedure : "retrieveStringFromBackend",
		parameters : [dummyInput]
	};

	WL.Client.invokeProcedure(invocationData, {
		onSuccess : adapterCallSuccessFn,
		onFailure : adapterCallFailureFn
	});

}

function firePlugin() {
	console.log("Plugin about to  F I R E !!");
	invokeSamplePlugin(pluginSuccessCallback, pluginFailureCallback);
}


/// private ///

function adapterCallSuccessFn(response) {
	var resultMsg = "Sample service succesfully invoked:\n" + JSON.stringify(response.invocationResult.serviceResponse.message,null,4);
	console.log(resultMsg);
	$('#result-msg').html(resultMsg);

	$('#result-msg').show();
}

function adapterCallFailureFn(response) {
	console.log("Error callback was invoked, response is " + JSON.stringify(response,null,4));
}

function invokeSamplePlugin(successCallback, errorCallback) {
	  if (typeof errorCallback != "function") {
	    console.log("Sample plugin failure: errorCallback parameter must be a function");
	    return
	  }

	  if (typeof successCallback != "function") {
	    console.log("Sample plugin failure: successCallback parameter must be a function");
	    return
	  }
	  cordova.exec(successCallback, errorCallback, "SamplePlugin", "hello", []);
}


function pluginSuccessCallback(result) {
	console.log("Plugin success callback was invoked");
	var resultMsg = "";
	try{
		resultMsg = JSON.stringify(result, null, 4);
	}
	catch(e) {
		resultMsg = "NO ERROR MSG";
	}
	$('#result-msg').html("Sample plugin successfully executed:\n" + resultMsg);

	$('#result-msg').show();
}

function pluginFailureCallback(result) {
	console.log("Plugin failure callback was invoked");
	var errorMsg = "";
	try{
		errorMsg = JSON.stringify(result, null, 4);
	}
	catch(e) {
		errorMsg = "NO ERROR MSG";
	}
	$('#result-msg').html("Sample plugin somehow failed executing:\n" + errorMsg);
	$('#result-msg').show();
}

function onConnectSuccess() {
	console.log("Successfully connected to the server!");
}

function onConnectFailure() {
	console.log("Failed connecting to the server...");
}