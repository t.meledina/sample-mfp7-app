package com.NewSampleHybridApp.cordova.plugins;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

public class SamplePlugin extends CordovaPlugin {

    private final String MESSAGE_FROM_THE_PLUGIN = "This is a message from the plugin, it's written within the Java code and logged through System.out.println";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("hello")) {
            try {
                System.out.println(MESSAGE_FROM_THE_PLUGIN);
                callbackContext.success(MESSAGE_FROM_THE_PLUGIN);
                return true;
            } catch (Exception e) {
                callbackContext.error(e.getMessage());
                return false;
            }
        }
        return false;
    }

}
